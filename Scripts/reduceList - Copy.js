﻿function nullHendel(value)
{
    return value ? value : "";
}

$(document).ready(function () {

    $("#finialSave").on("click", function () {
        $("#message").text("האם אתה בטוח?");
        $("#message").dialog(
              {
                  buttons: {
                      סגור: function () {
                          $(this).dialog("close");
                      },
                      כן: function () {
                          $(this).dialog("close");
                          $("#tr_" + id).css("background-color", "white");
                          var data = [];
                          $("tbody tr").each(function () {
                              data.push(
                                $(this).attr("id")
                              );

                              $.ajax({
                                  type: "POST",
                                  url: "/Home/SaveAllFile",
                                  contentType: "application/json; charset=utf-8",
                                  data: JSON.stringify(data),
                                  dataType: "json",
                                  success: function (data, status, jqXHR) {
                                      $("#message").text("נשמר בהצלחה");
                                      $("#message").dialog(
                                            {
                                                buttons: {
                                                    סגור: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            });

                                  }
                              });
                          });


                      }
                  }
              });
               
    });
       
    var url = "/Home/GetListForReduce/" + $("#num_torem").val();

    $.get(url, function (data) {
        for (var i = 0; i < data.length; i++) {
            var date = "";
            if (data[i].DateNextKesher) {
                var milli = data[i].DateNextKesher.replace(/\/Date\((-?\d+)\)\//, '$1');
                var datet = new Date(parseInt(milli));
                date = datet.getDate() + '/' + (datet.getMonth() + 1) + '/' + datet.getFullYear();
            }
            var telephones = "";
            if (data[i].TelEr != null && data[i].TelEr != "")
            {
                var tels = data[i].TelEr.trim().split(' ');
               for (var j = 0; j < tels.length; j++)
                {
                    if (tels[j] != "")
                    {
                    telephones += "<a href='tel:" + tels[j] + "'>" + tels[j] + "</a> ";
                }
                }
            }
            var value = "<tr id='" + data[i].numerua + "' >" +
               "<td>" + nullHendel(data[i].LastNameEr) + " " + nullHendel(data[i].FirstNameEr) + "</td>" +

               "<td><b>" + telephones + "</b></td>" +
               "<td>" + nullHendel(data[i].Teur) + "</td>" +
               "<td ><input type='text' class='DateNextKesher form-control' value='" + date + "' /></td>" +
              " <td><textarea   class='tguvaEr form-control' >" + nullHendel(data[i].tguvaEr) + "</textarea></td>" +
           "</tr>";
            $("#dataTable table tbody").append(value);
        }

        $(".DateNextKesher").datepicker({
            dateFormat: "dd/mm/y",
            dayNamesMin: ["א", "ב", "ג", "ד", "ה", "ו", "ש"],
            monthNames: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"]
        });

        $('.form-control').on('focus', function () {
            $(this).parent().parent().attr("changed", true);
        });
        $('.form-control').on('change', function () {           
            var _this = $(this);
            var data = [];
            data.push({
                DateNextKesher: $(this).parent().parent().find(".DateNextKesher").val(),
                tguvaEr: $(this).parent().parent().find(".tguvaEr").val(),
                numerua: $(this).parent().parent().attr("id")
            });

            $.ajax({
                type: "POST",
                url: "/Home/SaveReduceList",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data, status, jqXHR) {
                    $("#message").text("נשמר");
                    _this.parent().parent().removeAttr("changed");
                }
            });
        });
     
     });
    $(window).bind("beforeunload", function () {
        $("tbody tr[changed=true]").each(function () {
            var data = [];
            data.push({
                DateNextKesher: $(this).find(".DateNextKesher").val(),
                tguvaEr: $(this).find(".tguvaEr").val(),
                numerua: $(this).attr("id")
            });

            $.ajax({
                type: "POST",
                url: "/Home/SaveReduceList",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data, status, jqXHR) {
                    $("#message").text("נשמר");
                }
            });
        });

    });
   
});

//$( window ).unload(function() {
   
//    alert("האם לשמור את השינויים עד כה?");
//    $("tr[changed=true]").each(function () {
//        var data = [];
//            data.push({
//                DateNextKesher: $(this).find(".DateNextKesher").val(),
//                tguvaEr: $(this).find(".tguvaEr").val(),
//                numerua: $(this).attr("id")
//            });

//            $.ajax({
//                type: "POST",
//                url: "/Home/SaveReduceList",
//                contentType: "application/json; charset=utf-8",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data, status, jqXHR) {
//                    $("#message").text("נשמר");
//                }
//            });
//        });
     
//    });

 


 

  

