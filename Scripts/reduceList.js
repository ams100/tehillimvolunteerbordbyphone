﻿function nullHendel(value)
{
    return value ? value : "";
}

$(document).ready(function () {

    $("#finialSave").on("click", function () {
        $("#message").text("האם אתה בטוח?");
        $("#message").dialog(
              {
                  buttons: {
                      סגור: function () {
                          $(this).dialog("close");
                      },
                      כן: function () {
                          $(this).dialog("close");
                          //$("#tr_" + id).css("background-color", "white");
                          var data = [];
                          $("div.detailDiv").each(function () {
                              data.push(
                                $(this).attr("id")
                              );

                              $.ajax({
                                  type: "POST",
                                  url: "/Home/SaveAllFile",
                                  contentType: "application/json; charset=utf-8",
                                  data: JSON.stringify(data),
                                  dataType: "json",
                                  success: function (data, status, jqXHR) {
                                      $("#message").text("נשמר בהצלחה");
                                      $("#message").dialog(
                                            {
                                                buttons: {
                                                    סגור: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            });

                                  }
                              });
                          });


                      }
                  }
              });
               
    });
       
    var url = "/Home/GetListForReduce/" + $("#num_torem").val();

    $.get(url, function (data) {
        for (var i = 0; i < data.length; i++) {
            var date = "";
            if (data[i].DateNextKesher) {
                var milli = data[i].DateNextKesher.replace(/\/Date\((-?\d+)\)\//, '$1');
                var datet = new Date(parseInt(milli));
                date = datet.getDate() + '/' + (datet.getMonth() + 1) + '/' + datet.getFullYear();
            }
            var telephones = "";
            if (data[i].TelEr != null && data[i].TelEr != "")
            {
                var tels = data[i].TelEr.trim().split(' ');
               for (var j = 0; j < tels.length; j++)
                {
                    if (tels[j] != "")
                    {
                    telephones += "<a href='tel:" + tels[j] + "'>" + tels[j] + "</a> ";
                }
                }
            }

            var value = "<div class='detailDiv' id='" + data[i].numerua + "' >" +
                           "<div class='row bigfont'><div  class='col-md-4'><label >שם:</label>" + nullHendel(data[i].LastNameEr) + " " + nullHendel(data[i].FirstNameEr) + "</div>" +
                           "<div class='col-md-4 bigfont'><label>טלפון:</label>" + telephones + "</div>" +
                           "<div class='col-md-4'><label>תאריך:</label><input type='text' class='DateNextKesher form-control' value='" + date + "' /></div></div>" +

                           "<div class='row'><div class='col-md-12'><label>תאור:</label>" + nullHendel(data[i].Teur) + "</div></div>" +
                           "<div class='row'><div class='col-md-12'><label>תגובה:</label><textarea   class='tguvaEr form-control' >" + nullHendel(data[i].tguvaEr) + "</textarea></div></div>" +
                       "</div>";
             $("#dataTable").append(value);
        
        }

        $(".DateNextKesher").datepicker({
            dateFormat: "dd/mm/yy",
            dayNamesMin: ["א", "ב", "ג", "ד", "ה", "ו", "ש"],
            monthNames: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"]
        });

        $('.form-control').on('focus', function () {
            $(this).parent().parent().parent().attr("changed", true);
        });
        $('.form-control').on('change', function () {           
            var _this = $(this);
            var data = [];
            data.push({
                DateNextKesher: $(this).parent().parent().parent().find(".DateNextKesher").val(),
                tguvaEr: $(this).parent().parent().parent().find(".tguvaEr").val(),
                numerua: $(this).parent().parent().parent().attr("id")
            });

            $.ajax({
                type: "POST",
                url: "/Home/SaveReduceList",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data, status, jqXHR) {
                    $("#message").text("נשמר");
                    _this.parent().parent().parent().removeAttr("changed");
                }
            });
        });
     
     });
    $(window).bind("beforeunload", function () {
        $("div[changed=true]").each(function () {
            var data = [];
            data.push({
                DateNextKesher: $(this).find(".DateNextKesher").val(),
                tguvaEr: $(this).find(".tguvaEr").val(),
                numerua: $(this).attr("id")
            });

            $.ajax({
                type: "POST",
                url: "/Home/SaveReduceList",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data, status, jqXHR) {
                    $("#message").text("נשמר");
                }
            });
        });

    });
   
});

//$( window ).unload(function() {
   
//    alert("האם לשמור את השינויים עד כה?");
//    $("tr[changed=true]").each(function () {
//        var data = [];
//            data.push({
//                DateNextKesher: $(this).find(".DateNextKesher").val(),
//                tguvaEr: $(this).find(".tguvaEr").val(),
//                numerua: $(this).attr("id")
//            });

//            $.ajax({
//                type: "POST",
//                url: "/Home/SaveReduceList",
//                contentType: "application/json; charset=utf-8",
//                data: JSON.stringify(data),
//                dataType: "json",
//                success: function (data, status, jqXHR) {
//                    $("#message").text("נשמר");
//                }
//            });
//        });
     
//    });

 


 

  

