﻿$(document).ready(function () {
    $("#deleteFilter").jqxButton({
        width: '150'
    });
    $("#specialDayButton").jqxButton({
        width: '150'
    });
    $("#specialDayButton").on('click', function (event) {
        var url = "/PdfShow/PDF/1?userId=" + $("#num_torem").val();
        var win = window.open(url, '_blank');
        win.focus();
    });
    $("#messerButton").jqxButton({
        width: '150'
    });
    $("#messerButton").on('click', function (event) {
        var url = "/PdfShow/PDF/2?userId=" + $("#num_torem").val();
        var win = window.open(url, '_blank');
        win.focus();
    });
    function Showloadimage() {
        setTimeout('document.images["loadimage"].src = "/Content/ajax-loading.gif"', 200);
        $("#divLoading").show();
    }
    function printTable() {
     
        var url = "/Home/GetPrintPage/" + $("#num_torem").val();
        var win = window.open(url,'print', '_blank');
        win.focus();


    };
    function createSingleFieldDataAdapter(fieldName, fieldType, url) {
        var source = {
            dataType: "json",
            dataFields: [{
                name: fieldName,
                type: fieldType
            }
            ],
            url: url,
            id: fieldName
        };
        source.dataFields.unshift('(Select All)');
        return new $.jqx.dataAdapter(source);
    }

    function dateRenderer(index, label, value) {
        return value == null ? "לא מוגדר" : value.toLocaleDateString("HE").replace(/\./g, '/');
    }

    function nullRenderer(index, label, value) {
        return value || "לא מוגדר";
    }

    $("#statusFilter").jqxComboBox({
        source: createSingleFieldDataAdapter("StatusErua", "string", "/Home/StatusList"),
        displayMember: "StatusErua",
        valueMember: "StatusErua",
        checkboxes: true,
        width: '200',
        height: '25',
        autoDropDownHeight: true,
        renderer: nullRenderer,
        rtl: true
    });
    $("#nameFilter").jqxComboBox({
        source: createSingleFieldDataAdapter("LastNameEr", "string", "/Home/LastNameList"),
        displayMember: "LastNameEr",
        valueMember: "LastNameEr",
        checkboxes: true,
        width: '200',
        height: '25',
        autoDropDownHeight: true,
        renderer: nullRenderer,
        rtl: true
    });
    $("#fileFilter").jqxComboBox({
        source: createSingleFieldDataAdapter("NameFileEr", "string", "/Home/FileList"),
        displayMember: "NameFileEr",
        valueMember: "NameFileEr",
        checkboxes: true,
        width: '200',
        height: '25',
        autoDropDownHeight: true,
        renderer: nullRenderer,
        rtl: true
    });

    $("#DateFilter").jqxComboBox({
        source: createSingleFieldDataAdapter("DateNextKesher", "date", "/Home/NextDateList"),
        displayMember: "DateNextKesher",
        valueMember: "DateNextKesher",
        checkboxes: true,
        width: '200',
        height: '25',
        autoDropDownHeight: true,
        renderer: dateRenderer,
        rtl: true
    });

    $('#statusFilter').on('bindingComplete', function (event) {
        $('#statusFilter').jqxComboBox('checkAll');
        $('#statusFilter').data("bound", true);
        applyFiltersAndSetCheckChangeEvents();
    });

    $('#nameFilter').on('bindingComplete', function (event) {
        $('#nameFilter').jqxComboBox('checkAll');
        $('#nameFilter').data("bound", true);
        applyFiltersAndSetCheckChangeEvents();
    });

    $('#fileFilter').on('bindingComplete', function (event) {
        $('#fileFilter').jqxComboBox('checkAll');
        $('#fileFilter').data("bound", true);
        applyFiltersAndSetCheckChangeEvents();
    });

    $('#DateFilter').on('bindingComplete', function (event) {
        $('#DateFilter').jqxComboBox('checkAll');
        $('#DateFilter').data("bound", true);
        applyFiltersAndSetCheckChangeEvents();
    });

    $('#deleteFilter').on("click", function () {
        unsetCheckChangeEvents();
        $('#statusFilter').jqxComboBox('checkAll');
        $('#nameFilter').jqxComboBox('checkAll');
        $('#fileFilter').jqxComboBox('checkAll');
        $('#DateFilter').jqxComboBox('checkAll');
        applyFiltersAndSetCheckChangeEvents();
    });

    function updateFilterComboboxesBoundData() {
        $("#statusFilter").jqxComboBox({
            source: createSingleFieldDataAdapter("StatusErua", "string", "/Home/StatusList")
        });
        $("#nameFilter").jqxComboBox({
            source: createSingleFieldDataAdapter("LastNameEr", "string", "/Home/LastNameList")
        });
        $("#fileFilter").jqxComboBox({
            source: createSingleFieldDataAdapter("NameFileEr", "string", "/Home/FileList")
        });
        $("#DateFilter").jqxComboBox({
            source: createSingleFieldDataAdapter("DateNextKesher", "date", "/Home/NextDateList")
        });
    }

    function applyFiltersAndSetCheckChangeEvents() {
        if ($('#statusFilter').data("bound") && $('#nameFilter').data("bound") && $('#fileFilter').data("bound") && $('#DateFilter').data("bound")) {
            applyFilter("StatusErua", "stringfilter", "statusFilter");
            applyFilter("LastNameEr", "stringfilter", "nameFilter");
            applyFilter("NameFileEr", "stringfilter", "fileFilter");
            applyFilter("DateNextKesher", "datefilter", "DateFilter");
            $('#statusFilter').on('checkChange', function (event) {
                applyFilter("StatusErua", "stringfilter", "statusFilter");
            });

            $('#nameFilter').on('checkChange', function (event) {
                applyFilter("LastNameEr", "stringfilter", "nameFilter");
            });

            $('#fileFilter').on('checkChange', function (event) {
                applyFilter("NameFileEr", "stringfilter", "fileFilter");
            });

            $('#DateFilter').on('checkChange', function (event) {
                applyFilter("DateNextKesher", "datefilter", "DateFilter");
            });
        }
    }

    function unsetCheckChangeEvents() {
        $('#statusFilter').off('checkChange', '**');

        $('#nameFilter').off('checkChange', '**');

        $('#fileFilter').off('checkChange', '**');

        $('#DateFilter').off('checkChange', '**');
    }

    function applyFilter(datafield, filtertype, filterbox) {
        // create a new group of filters.
        var filtergroup = new $.jqx.filter();
        // get listbox's checked items.
        var topitems = [];
        var isDate = false;
        var checkedItems = $("#" + filterbox).jqxComboBox('getCheckedItems');
        if (checkedItems.length == 0) {
            var filter_or_operator = 1;
            var filtervalue = "Empty";
            var filtercondition = 'equal';
            var filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
            filtergroup.addfilter(filter_or_operator, filter);
        } else {
            for (var i = 0; i < checkedItems.length; i++) {
                var filter_or_operator = 1;
                // set filter's value.
                var filtervalue = checkedItems[i].label;
                // set filter's condition.
                var filtercondition = 'equal';
                // create new filter.
                var filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
                // add the filter to the filter group.
                filtergroup.addfilter(filter_or_operator, filter);
                if (typeof filtervalue == "object") {
                    topitems.push(filtervalue != null ? filtervalue.toLocaleDateString("HE").replace(/\./g, '/')  : "לא מוגדר" );
                    isDate = true;
                }
            }
        }
        $("#dataTable").jqxDataTable('removeFilter', datafield);
        // add the filters.
        $("#dataTable").jqxDataTable('addFilter', datafield, filtergroup);
        // apply the filters.
        $("#dataTable").jqxDataTable('applyFilters');

        if (isDate) {
            setTimeout(function () {
                $('#' + filterbox + ' .jqx-combobox-input').val(topitems.join());
            }, 100);
        }
    }

    function expandCollapseRows() {
        var method = $("#collapseAll").val() ? "hideDetails" : "showDetails";
        var rows = $("#dataTable").jqxDataTable("getRows");
        for (var i = 0; i < rows.length; i++) {
            $("#dataTable").jqxDataTable(method, i);
        }
    };

    $("#collapseAll").jqxCheckBox({
        width: 20,
        height: 25,
        checked:true
    });
    $("#ifSendMeneger").jqxCheckBox({
        width: 40,
        height: 25
    });

    $("#collapseAll").on('change', function (event) {
        expandCollapseRows();
    });
    $("#notSendReportDialog").jqxWindow({
        resizable: false,
        rtl: true,
        width: 400,
        height: 150,
        autoOpen: false,
        title:'התרעה'
    });
    
    //.jqxNotification({
    //    position: "bottom-left",
    //    appendContainer: "#container1",
    //    opacity: 1,
    //    width: 500,
    //    autoOpen: false, animationOpenDelay: 10, autoClose: true, autoCloseDelay: 3000, template: "info",
    //    theme: 'ui-redmond',
    //    rtl: true
    //});
    //$("#notSendReportDialog").on("open", function () {
    //    $("#container1").css({ "z-index": 9999 });
    //});
    //$("#notSendReportDialog").on("close", function () {
    //    $("#container1").css({ "z-index": -1 });
    //});
    $("#sendReportDialog").jqxWindow({
        resizable: false,
        rtl: true,
        width: 400,
        height: 200,
        autoOpen: false
    });
    $("#message").jqxWindow({
        resizable: false,
        rtl: true,
        width: 400,
        height: 200,
        autoOpen: false
    });
  
    $("#dialog").jqxWindow({
        resizable: false,
        rtl: true,
        width: 870,
        height: 900,
        autoOpen: false
    });

    var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
        switch (value) {
            case "סיים":
                return '<span style=" color: green;">' + value + '</span>';
            case "חדש":
                return '<span style=" color: red;">' + value + '</span>';
            default:
                return '<span style="color: blue;">' + value + '</span>';
        }
    };

    var index = 0;
    var initRowDetails = function (id, row, element, rowinfo) {
        var details = null;
        var information = null;
        var notes = null;
        // update the details height.
        rowinfo.detailsHeight = 150;
        element.append($("<div class='information' tabindex=" + index + " id='" + id + "'></div>"));
        index++;
        information = $(element.children()[0]);

        if (information != null) {
            var container = $('<div style="margin: 5px;diraction:rtl;text-align:right"></div>')
            container.appendTo($(information));
         
            var divSecond = $('<div class="second" style=""></div>');
            var div2 = $('<div class="div2Class"  style="float:right;width:70%" ></div>');
            var div3 = $('<div class="div3Class"  style="float:right;width:30%" ></div>');
           
            container.append(divSecond);
            divSecond.append(div2);
            divSecond.append(div3);
         

            var div2Content = '<div>' + (row.Teur || "") + '</div><div><label style="float:right;width:80px">:תגובה</label>' + (row.tguvaEr || "") + '</div>';


            $(div2).append(div2Content);


            var div3Content = '<div><label style="float:right;width:200px">:תאריך ליצירת קשר</label>' +
                (row.DateNextKeshe != null ? row.DateNextKesher.toLocaleDateString("HE").replace(/\./g, '/') : "") +
                '</div><div><label style="float:right;width:200px"> :תאריך שיחה אחרונה</label>' + (row.DateLastKesher != null ? row.DateLastKesher.toLocaleDateString("HE").replace(/\./g, '/') : "") + "</div>";
            $(div3).append(div3Content);


          
      
        }
    }

    // prepare the data
    var source = {
        dataType: "json",
        dataFields: [{
            name: 'Eara',
            type: 'string'
        }, {
            name: 'LastNameEr',
            type: 'string'
        }, {
            name: 'FirstNameEr',
            type: 'string'
        }, {
            name: 'TelEr',
            type: 'string'
        }, {
            name: 'Teur',
            type: 'string'
        }, {
            name: 'NameFileEr',
            type: 'string'
        }, {
            name: 'DateNextKesher',
            type: 'date'
        }, {
            name: 'tguvaEr',
            type: 'string'
        }, {
            name: 'StatusErua',
            type: 'string'
        }, {
            name: 'DateLastKesher',
            type: 'date'
        },
        {
            name: 'DateRegSiha',
            type: 'date'
        }
        ],
        url: "/Home/AjaxList/",
        id: 'numerua'
    };

    var dataAdapter = new $.jqx.dataAdapter(source);

    $("#dataTable").jqxDataTable({
        width: 1190,
        rtl: true,
        source: dataAdapter,
        pageable: true,
        pageSize: 15,
        rowDetails: true,
        sortable: true,
        theme: 'ui-redmond',
        selectionMode: "singleRow",
        ready: function () {

            // called when the DataTable is loaded.
            // init jqxWindow widgets.
          

            var ddSource = ["סיים", "מעקב", "חדש"];
            $("#DStatusErua").jqxDropDownList({
                source: ddSource,
                selectedIndex: 1,
                width: '200',
                height: '25',
                rtl: true
            });
            $("#DStatusErua").on('change', function (event) {
                var args = event.args;
                if (args) {

                    var item = args.item;

                    var value = item.value;
                    if (value == "מעקב") {
                        $("#divDateNextKesher").show();
                    } else {
                        $("#divDateNextKesher").hide();
                    }
                }
            });
            $("#DDateNextKesher").jqxDateTimeInput({
                rtl: true,
                formatString: 'dd/MM/yyyy',
                width: 150,
                height: 30
            });
            $("#DDateRegSiha").jqxDateTimeInput({
                rtl: true,
                formatString: 'dd/MM/yyyy',
                width: 150,
                height: 30
            });

            $("#save").jqxButton({
                height: 30,
                width: 80
            });
            $("#cancel").jqxButton({
                height: 30,
                width: 80
            });
            $("#cancel").on("click", function () {
                // close jqxWindow.
                $("#dialog").jqxWindow('close');
            });
            $("#closeNotSendReportDialog").on("click", function () {
                // close jqxWindow.
                $("#notSendReportDialog").jqxWindow('close');
            });

            function updateDetails(id, row) {
                var element = $("div.information#" + id);

                var information = $(element.children()[0]);

                if (information != null) {

                    var container = $('<div style="margin: 5px;" class="second"></div>');
                    $("div.information#" + id + " div.second").replaceWith(container);
                    //var div1 = $('<div class="div1Class" style="float:right;width:40%" ></div>');
                    var div2 = $('<div class="div2Class"  style="float:right;width:70%" ></div>');
                    var div3 = $('<div class="div3Class"  style="float:right;width:30%" ></div>');
                    //container.append(div1);
                    container.append(div2);
                    container.append(div3);
                 
                    var div2Content = '<div>' + (row.Teur || "") + '</div><div><label style="float:right;width:80px">:תגובה</label>' + (row.tguvaEr || "") + '</div>';


                    $(div2).append(div2Content);


                    var div3Content = '<div><label style="float:right;width:200px">:תאריך ליצירת קשר</label>' +
                        (row.DateNextKesher || "") +
                        '</div><div><label style="float:right;width:200px"> :תאריך שיחה אחרונה</label>' + (row.DateLastKesher || "") + "</div>";
                    $(div3).append(div3Content);
                }
            }

            $("#save").on("click", function () {
              
                if ($("#DStatusErua").val() == "חדש") {
                    var confirmAlert = confirm("שים לב הרשומה עדיין על חדש ​");
                    if (!confirmAlert)
                    { return }
                }
                //close jqxWindow.
                $("#dialog").jqxWindow('close');
                  Showloadimage();
                // update edited row.
                var rowId = parseInt($("#dialog").attr('data-row-id'));
                $.ajax({
                    type: "POST",
                    url: "/Home/Update/",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        id: rowId,
                        StatusErua: $("#DStatusErua").val(),
                        DateNextKesher: $("#DDateNextKesher").val(),
                        DateRegSiha: $("#DDateRegSiha").val(),
                        tguvaEr: $("#DtguvaEr").val(),
                        sendToMenager: $("#ifSendMeneger").val()
                    }),
                    dataType: "json",
                    success: function (data, status, jqXHR) {
                        $("#divLoading").hide();
                        if (data.message != "") {
                            $("#mesaageText").text(data.message);
                            $("#message").jqxWindow('open');
                            var x = ($(window).width() - $("#sendReportDialog").jqxWindow('width')) / 2 + $(window).scrollLeft();
                            var y = ($(window).height() - $("#sendReportDialog").jqxWindow('height')) / 2 + $(window).scrollTop();
                            $("#message").jqxWindow({
                                position: {
                                    x: x,
                                    y: y
                                },
                              
                                   title:"הודעה"                        });
                            $("#message").css('visibility', 'visible');

                        }
                        if (data.success) {
                            var rowData = {
                                StatusErua: $("#DStatusErua").val(),
                                DateNextKesher: $("#DDateNextKesher").val(),
                                tguvaEr: $("#DtguvaEr").val(),
                                DateRegSiha: $("#DDateRegSiha").val()
                             

                            };
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!
                            var yyyy = today.getFullYear();

                            if (dd < 10) {
                                dd = '0' + dd
                            }

                            if (mm < 10) {
                                mm = '0' + mm
                            }

                            today = dd + '/' + mm + '/' + yyyy;
                            var updateRowData = {
                                StatusErua: $("#DStatusErua").val(),
                                DateNextKesher: $("#DStatusErua").val() == "מעקב"?  $("#DDateNextKesher").val() : "",
                                tguvaEr: $("#DtguvaEr").val(),
                                FirstNameEr: $("#fName").val(),
                                LastNameEr: $("#Lname").val(),
                                Eara: $("#Eara").val(),
                                TelEr: $("#TelEr").val(),
                                PelEr: $("#PelEr").val(),
                                Sivug: $("#Sivug").val(),
                                NameFileEr: $("#NameFileEr").val(),
                                Teur: $("#DTeur").val(),
                                DateLastKesher: today,
                                DateRegSiha: $("#DDateRegSiha").val()
                            };
                            var key = selectedDetails.attr("id");
                            var row = $("#dataTable #contenttabledataTable tr[data-key=" + key + "]");
                            var allRows = $("#dataTable #contenttabledataTable tr[role='row']");
                            var index = allRows.index(row) + currentPage * $("#dataTable").jqxDataTable('pageSize');

                            $("#dataTable").jqxDataTable('updateRow', index, updateRowData);
                            updateDetails(rowId, updateRowData);
                        }
                    }
                });
                //updateFilterComboboxesBoundData();
            });
            $("#dialog").on('close', function () {
                // enable jqxDataTable.
                $("#dataTable").jqxDataTable({
                    disabled: false
                });
            });

        },
        pagerButtonsCount: 8,
        initRowDetails: initRowDetails,
        //rendered: function () {
        //    if ($(".edit").length > 0) {
        //        //$(".edit").jqxButton();
        //        $(".edit").on('click', function (event) {
                   
        //            var id = parseInt(event.target.getAttribute('id').split('_')[1]);
        //            var row1 = $("#dataTable #contenttabledataTable tr[data-key=" + id + "]");
        //            var allRows = $("#dataTable #contenttabledataTable tr[role='row']");
        //            var rows = $("#dataTable").jqxDataTable('getRows');
        //            var index = allRows.index(row1) + currentPage * $("#dataTable").jqxDataTable('pageSize');
        //            var row = rows[index];
        //            selectedDetails = $("#dataTable div#" + id + ".information");
        //            $("#dialog").jqxWindow('setTitle', "ערוך שורה: " + (row.LastNameEr || "") + " " + (row.FirstNameEr || ""));
        //            var x = ($(window).width() - $("#dialog").jqxWindow('width')) / 2 + $(window).scrollLeft();
        //            var y = ($(window).height() - $("#dialog").jqxWindow('height')) / 2 + $(window).scrollTop();
        //            $("#dialog").jqxWindow({
        //                position: {
        //                    x: x,
        //                    y: y
        //                }
        //            });
        //            $("#dialog").jqxWindow('open');
        //            $("#dialog").attr('data-row-id', id);
        //            $("#dataTable").jqxDataTable({
        //                disabled: true
        //            });
        //            $("#Dname").val((row.LastNameEr || "") + " " + (row.FirstNameEr || ""));
        //            $("#DTeur").val(row.Teur || "");
        //            $("#DtguvaEr").val(row.tguvaEr || "");
        //            $("#DStatusErua").val(row.StatusErua || "");
        //            $("#dialog").css('visibility', 'visible');
        //            $("#fName").val(row.FirstNameEr || "");
        //            $("#Lname").val(row.LastNameEr || "");
        //            $("#Eara").val(row.Eara || "");
        //            $("#TelEr").val(row.TelEr || "");
        //            $("#PelEr").val(row.PelEr || "");
        //            $("#Sivug").val(row.Sivug || "");
        //            $("#NameFileEr").val(row.NameFileEr || "");
        //            $("#ifSendMeneger").val(false);
        //            if (row.StatusErua == "מעקב") {
        //                $("#DDateNextKesher").val(row.DateNextKesher);
        //                $("#divDateNextKesher").show();
        //            }

                  

        //                $("#dataTable").jqxDataTable('showDetails',index);
        //                $("#dataTable .information").attr("selectedDetails", false);
        //                if (selectedDetails)
        //                    selectedDetails.removeAttr("selectedDetails");
        //                selectedDetails = $("#dataTable div#" + id + ".information");
        //                selectedDetails.attr("selectedDetails", true);
        //            //$("#dataTable").jqxDataTable('selectRow', index);
        //        });
        //    }
        //},
        
        columns: [
            {
                text: 'עריכה', cellsAlign: 'center', align: "center", columnType: 'none', editable: false, sortable: false, dataField: null, cellsRenderer: function (row, column, value, defaultHtml){           
                    return "<input type='button' id='button_" + defaultHtml.uid + "' class='edit' value='עריכה'/>";
                },
                width:100
            },
            {
            text: '',
            dataField: 'Eara',
            width: 100,
        }, {
            text: 'שם משפחה',
            dataField: 'LastNameEr',
            width: 155,
            align: 'center',
            cellsalign: 'center'
        }, {
            text: 'שם פרטי',
            dataField: 'FirstNameEr',
            width: 150,
            align: 'center',
            cellsalign: 'center'
        }, {
            text: 'טלפון',
            dataField: 'TelEr',
            width: 200,
            align: 'center',
            cellsalign: 'center'
        }, {
            text: 'קובץ',
            dataField: 'NameFileEr',
            width: 100,
            align: 'center',
            cellsalign: 'center'
        }, {
            text: 'תאריך ליצירת קשר',
            dataField: 'DateNextKesher',
            width: 150,
            align: 'center',
            cellsalign: 'center',
            cellsformat: 'd/M/yy'
        }, {
            text: 'סטטוס',
            dataField: 'StatusErua',
            width: 140,
            align: 'center',
            cellsalign: 'center',
            cellsrenderer: cellsrenderer
        }, {
            text: '',
            dataField: 'DateRegSiha',
            width: 0,
            cellsformat: 'd/M/yy',
            cellclassname: "whiteCell"
        },
            {
                text: '',
                dataField: 'DateLastKesher',
                width: 0,
                cellsformat: 'd/M/yy',
                cellclassname: "whiteCell"
             

            },
            {
                text: '',
                dataField: 'tguvaEr',
                width: 0,
                cellclassname: "whiteCell"
            }

        ]
    });
   
    $("#dataTable").jqxDataTable('hideColumn', 'DateLastKesher');
    $("#dataTable").jqxDataTable('hideColumn', 'tguvaEr');
  
    var currentPage = 0,
	selectedDetails;

    function selectAppropriateRow() {
        if (!selectedDetails)
            return;
        var key = selectedDetails.attr("id");
        var row = $("#dataTable #contenttabledataTable tr[data-key=" + key + "]");
        var allRows = $("#dataTable #contenttabledataTable tr[role='row']");
        var index = allRows.index(row) + currentPage * $("#dataTable").jqxDataTable('pageSize');
        $("#dataTable").jqxDataTable("selectRow", index);
    }

    //$('#dataTable').on('bindingComplete', function (event) {
    //    expandCollapseRows();
    //    bindInformationEvent();
    //    selectAppropriateRow();
    //    $("#dataTable").jqxDataTable('goToPage', currentPage);
    //    if (selectedDetails)
    //        $("html").scrollTo(selectedDetails, 1000, {
    //            offset: {
    //                top: -180
    //            }
    //        });
    //});

    $('#dataTable').on('pageChanged', function (event) {
        currentPage = event.args.pagenum;
        setTimeout(function () {
            expandCollapseRows();
            bindInformationEvent();
        }, 0);
    });

    $('#dataTable').on('rowSelect', function (event) {

        var id = event.args.key;
        $("#dataTable").jqxDataTable('showDetails', event.args.index);
        $("#dataTable .information").attr("selectedDetails", false);
        if (selectedDetails)
            selectedDetails.removeAttr("selectedDetails");
        selectedDetails = $("#dataTable div#" + id + ".information");
        selectedDetails.attr("selectedDetails", true);
      
                   
                  
        var row1 = $("#dataTable #contenttabledataTable tr[data-key=" + id + "]");
        var allRows = $("#dataTable #contenttabledataTable tr[role='row']");
        var rows = $("#dataTable").jqxDataTable('getRows');
        var index = allRows.index(row1) + currentPage * $("#dataTable").jqxDataTable('pageSize');
        var row = rows[index];
        selectedDetails = $("#dataTable div#" + id + ".information");
        $("#dialog").jqxWindow('setTitle', "ערוך שורה: " + (row.LastNameEr || "") + " " + (row.FirstNameEr || ""));
        var x = ($(window).width() - $("#dialog").jqxWindow('width')) / 2 + $(window).scrollLeft();
        var y = ($(window).height() - $("#dialog").jqxWindow('height')) / 2 + $(window).scrollTop();
        $("#dialog").jqxWindow({
            position: {
                x: x,
                y: y
            }
        });
        $("#dialog").jqxWindow('open');
        $("#dialog").attr('data-row-id', id);
        $("#dataTable").jqxDataTable({
            disabled: true
        });
        $("#DnameAndPhone").text((row.LastNameEr || "") + " " + (row.FirstNameEr || "") + " " + (row.TelEr || "") + " " + (row.PelEr || ""));
        $("#LastConverDate").text(row.LastNextKesher || "");
        $("#DDateRegSiha").val(row.DateRegSiha);
        
        $("#DTeur").val(row.Teur || "");
        $("#DtguvaEr").val(row.tguvaEr || "");
        $("#DStatusErua").val(row.StatusErua || "");
        $("#dialog").css('visibility', 'visible');
        $("#fName").val(row.FirstNameEr || "");
        $("#Lname").val(row.LastNameEr || "");
        $("#Eara").val(row.Eara || "");
        $("#TelEr").val(row.TelEr || "");
        $("#PelEr").val(row.PelEr || "");
        $("#Sivug").val(row.Sivug || "");
        $("#NameFileEr").val(row.NameFileEr || "");
        $("#ifSendMeneger").val(false);
        $("#DDateNextKesher").val(row.DateNextKesher);
        if (row.StatusErua == "מעקב") {
          
            $("#divDateNextKesher").show();
        }

                  

        $("#dataTable").jqxDataTable('showDetails',index);
        $("#dataTable .information").attr("selectedDetails", false);
        if (selectedDetails)
            selectedDetails.removeAttr("selectedDetails");
        selectedDetails = $("#dataTable div#" + id + ".information");
        selectedDetails.attr("selectedDetails", true);
        //$("#dataTable").jqxDataTable('selectRow', index);
    });
           


   
    function bindInformationEvent() {
        var infoDivs = $("#dataTable div.information");
        infoDivs.off("click", "**");
        infoDivs.on("click", function (event) {
            if (selectedDetails)
                selectedDetails.removeAttr("selectedDetails");
            selectedDetails = $(event.currentTarget);
            selectAppropriateRow();
            selectedDetails.attr("selectedDetails", true);
        });
    }

    $("#btnSendMonthReport").jqxButton();
    $("#print").jqxButton();
    $("#print").on('click', function () { printTable()})
    $('#btnSendMonthReport').on('click', function () {
        var rows = $("#dataTable").jqxDataTable('getRows');
        var isNew = false;
        for (var i = 0; i < rows.length;i++)
        {
            isNew = rows[i].StatusErua == "חדש";
            if (isNew) break;
        }
        if (isNew) {
            $("#notSendReportDialog").jqxWindow('open');
            var x = ($(window).width() - $("#notSendReportDialog").jqxWindow('width')) / 2 + $(window).scrollLeft();
            var y = ($(window).height() - $("#notSendReportDialog").jqxWindow('height')) / 2 + $(window).scrollTop();
            $("#notSendReportDialog").jqxWindow({
                position: {
                    x: x,
                    y: y
                }
               
            });
            $("#notSendReportDialog").css('visibility', 'visible');
            return;
        } else {
            $("#sendReportDialog").jqxWindow('open');
            var x = ($(window).width() - $("#sendReportDialog").jqxWindow('width')) / 2 + $(window).scrollLeft();
            var y = ($(window).height() - $("#sendReportDialog").jqxWindow('height')) / 2 + $(window).scrollTop();
            $("#sendReportDialog").jqxWindow({
                position: {
                    x: x,
                    y: y
                },
              
               
            });
            $("#sendReportDialog").css('visibility', 'visible');
        }
    });

    $("#sendCancel").mousedown(function () {
        // close jqxWindow.
        $("#sendReportDialog").jqxWindow('close');
    });

    $("#send").mousedown(function () {
        //close jqxWindow.
        $("#sendReportDialog").jqxWindow('close');
        // update edited row.

        $.ajax({
            type: "GET",
            url: "/Home/SendMailMonth?NameFiles=" + $("#fileFilter").jqxComboBox('getCheckedItems').map(function (i) { return  i.label ; }).join(),
            contentType: "application/json; charset=utf-8",

            dataType: "json",
            success: function (data) {
                $("#mesaageText").text(data);
                $("#message").jqxWindow('open');
                var x = ($(window).width() - $("#sendReportDialog").jqxWindow('width')) / 2 + $(window).scrollLeft();
                var y = ($(window).height() - $("#sendReportDialog").jqxWindow('height')) / 2 + $(window).scrollTop();
                $("#message").jqxWindow({
                    position: {
                        x: x,
                        y: y
                    },
                  
                });
                $("#message").css('visibility', 'visible');
               
            }
        });
    });

    function GetNewStatus() {

        var rows = $("[data-status]");
        var statuses = new Array();
        var contains = false;

        rows.each(function () {
            contains = false;
            var item = $(this);
            if ($(item).data("status").toString() == 'חדש') {
                contains = true;
            }

        });
        if (contains == true) {
            alert('רשומות בסטטוס חדש לא ישלחו למנהל מערכת');
        }

    }
  
});
