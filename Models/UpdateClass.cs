﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TehillimVulonteerBord.Models
{
    public class UpdateClass
    {
        public string StatusErua { set; get; }
        public string DateNextKesher { set; get; }
        public string tguvaEr { set; get; }
        public int id { set; get; }
        public bool? sendToMenager { set; get; }
        public string DateRegSiha { set; get; }
    }
}