﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace TehillimVulonteerBord.Models
{
    public class GmailSendMail
    {
        public string SendMail(string subject, string to, string body)
        {
            var fromAddress = new MailAddress("bbaa96771@gmail.com", "MyWebSite");
            var toAddress = new MailAddress(to, "Site Admin");
            string fromPassword = "bbaa96771bbaa96771";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }

            return "ok";

        }
    }
}