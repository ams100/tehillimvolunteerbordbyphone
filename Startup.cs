﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TehillimVolunteerBord.Startup))]
namespace TehillimVolunteerBord
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
