﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using TehillimVulonteerBord.Models;

namespace TehillimVulonteerBord.Controllers
{
    public class PdfShowController : Controller
    {
        tehillimEntities db = new tehillimEntities();
        public ActionResult PDF(int id, int userId)
        {
           
                var torem = db.tormim.First(p => p.num_torem == userId);
                ViewBag.UserName = torem.first_name + " " + torem.last_name;
                ViewBag.Title = id == 1 ? "ימים מיוחדים בחודש" : "מסר החודש";
            ViewBag.FileName = "pdf"+id + ".pdf";
            return View();
        }
      
	}
}